<!DOCTYPE html>
<html>
<head> 
<meta charset = "utf-8" />
<title>
Simple Calculator
</title>
</head>
<body>
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="GET">

First Number: <input name="number1" type="text"/> <br>
Second Number: <input name="number2" type="text"/> <br>
		
Function:
<input type="radio" name="function" value="add">Add
<input type="radio" name="function" value="subtract">Subtract
<input type="radio" name="function" value="multiply">Multiply
<input type="radio" name="function" value="divide">Divide
<br>

<input type="submit" name="submit" value="submit"/> <br>
</form>

<?php 
if(isset($_GET['submit'])){
	$num1 = $_GET['number1'];
	$num2 = $_GET['number2'];
	$func = $_GET['function'];
}
else {
	exit;
}

if($func == 'add'){
	echo $num1 + $num2;
}

else if($func == 'subtract'){
	echo $num1 - $num2;
}

else if($func == 'multiply'){
	echo $num1 * $num2;
}

else if($func == 'divide'){
	if($num2 == 0){
		echo "uh oh... can't do that";
	}
	else{
	echo $num1 /$num2;
}
}
?>

</body>
</html>